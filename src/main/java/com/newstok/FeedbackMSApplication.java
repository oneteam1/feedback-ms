package com.newstok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FeedbackMSApplication {

	public static void main(String[] args) {
		SpringApplication.run(FeedbackMSApplication.class, args);
	}

}

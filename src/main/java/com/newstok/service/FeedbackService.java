package com.newstok.service;

import com.newstok.model.Feedback;

@FunctionalInterface
public interface FeedbackService {
	void saveFeedback(Feedback feedback);
}

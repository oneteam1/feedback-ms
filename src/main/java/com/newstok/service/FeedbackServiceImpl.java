package com.newstok.service;

import java.util.Date;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.newstok.dao.FeedbackDao;
import com.newstok.mapper.FeedbackMapper;
import com.newstok.model.Feedback;
import com.newstok.model.FeedbackMessage;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@AllArgsConstructor
@EnableBinding(Source.class)
public class FeedbackServiceImpl implements FeedbackService{

	private DynamoDBMapper mapper;

	private Source source;

	@Override
	public void saveFeedback(Feedback feedback) {
		log.info("saveFeedback start ");
		
		FeedbackDao feedbackDao = FeedbackMapper.INSTANCE.objectToDao(feedback);
		feedbackDao.setTimestamp(new Date().toString());
		
		log.info("INCOMING FEEDBACK : " + feedbackDao.toString());
		
		try {
	    	mapper.save(feedbackDao);
	    } catch (Exception e) {
	    	log.error("Error saving FEEDBACK : " + feedbackDao.toString());
	    	e.printStackTrace();
	    	throw new InternalError(e.getMessage());
		}
		publishMessage(feedback);
		
		log.info("saveFeedback end ");
	}

	public void publishMessage(Feedback feedback) {
		log.info("publishMessage Start ");

		FeedbackMessage message = FeedbackMapper.INSTANCE.objectToMessage(feedback);

		log.info("PUBLISHING MESSAGE : " + message.toString());

		source.output().send(MessageBuilder.withPayload(feedback).build());

		log.info("publishMessage End ");
	}
}

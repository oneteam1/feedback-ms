package com.newstok.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.newstok.dao.FeedbackDao;
import com.newstok.model.Feedback;
import com.newstok.model.FeedbackMessage;

@Mapper
public interface FeedbackMapper {
	
	FeedbackMapper INSTANCE = Mappers.getMapper( FeedbackMapper.class );
	public FeedbackDao objectToDao(Feedback feedback);
	public FeedbackMessage objectToMessage(Feedback feedback);
}

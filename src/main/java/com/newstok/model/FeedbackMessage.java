package com.newstok.model;

import java.util.UUID;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class FeedbackMessage {
	UUID id = UUID.randomUUID();
	String newsId;
	String reportedId;
	String userId;
	String feedback;
	String timestamp;
}
